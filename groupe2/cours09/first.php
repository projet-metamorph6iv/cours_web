<?php
$maVariable = 2;
$maVariable = 6;

// commentaire sur une ligne

# ancienne façon de faire un commentaire

/*
commentaire 
sur 
plusieurs lignes
*/

$resultat = $maVariable * 4;
echo($resultat);
// * - + / %

$pseudo = "Sobraz";
// concaténation
echo("</br>Bonjour ".$pseudo);

// == != > < >= <=
// or || 	and &&
$age = 21;
if( $age >= 18/*$age > 18 || $age == 18*/ ){
	echo("</br>il est majeur !");
	$mineur = false;
}
else{
	echo("</br>il est mineur");
	$mineur = true;
}
// !
if(!$mineur){
	echo("</br>il est majeur j'ai dit !");
}

// ++ -- 
$number = 1;
$number++; // augmenter la veuleur de number de 1 (+1)
// incrémentation

// -= += *= /=
$number += 3; // $number = $number +3
echo("</br>");
for($num = 0;$num <= 1000;$num+=3){
	echo(" ".$num);
}

?>