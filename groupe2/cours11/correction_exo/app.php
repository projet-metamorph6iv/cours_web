<!DOCTYPE html>
<?php
session_start();
/*
$_SESSION = [
    'personnes' => ["Toto"]
]
*/
if(!isset($_SESSION['personnes'])){
    $personnes = [];
    $_SESSION['personnes'] = $personnes;
}else{
    $personnes = $_SESSION['personnes'];
}

if(isset($_POST['prenom'])){
    $p = $_POST['prenom'];                  // $p = "Titi"
    $_SESSION['personnes'][] = $p;

    $personnes = $_SESSION['personnes'];
    $personnes[] = $p;                      // $personnes = ["Toto","Titi"]
                                            // $_SESSION['personnes'] = ["Toto"]
    $_SESSION['personnes'] = $personnes;    // $_SESSION['personnes'] = ["Toto","Titi"]
}
?>

<html>
    <body>
        <form method="post">
            <fieldset>
                <input type="text" name="prenom"/>
                <input type="submit" value="Save"/>
            </fieldset>
        </form>

        <table>
            <thead>
                <tr>
                    <th>Prénom</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($personnes as $p){
                    echo("<tr><td>".$p."</td></tr>");
                }?>
            </tbody>
        </table>
    </body>
</html>
