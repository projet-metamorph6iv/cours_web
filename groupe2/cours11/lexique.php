<?php

// ############ isset ############

/*
sert à vérifier qu'une key existe dans un tableau
si la key existe => renvoie true
sinon => false
*/

$tableau = ["fruit" => "pomme", "legume" => "haricot"];

if(isset($tableau["fruit"])){
    echo("il y a un fruit");
}

if(isset($tableau["pokemons"])){
    echo("pokemon");
}

// ############ foreach tableau simple ############

$pokemons = ["Pikachu","Axoloto","Mew"];

// /!\ avant la boucle $p n'existe pas encore  /!\

foreach($pokemons as $p){  // on désigne $p comme la variable prise à chaque élément du tableau
    // à chaque tour de boucle $p prend la valeur du pokemon suivant
    echo("<br/>".$p);
}

// /!\ après la boucle $p n'existe PLUS  /!\

/*
 *
 ------- affichage ------

Pikachu
Axoloto
Mew

*/

// ############ foreach tableau key => valeur ############

$abricot = [
    "nom" => "abricot",
    "couleur" => "orange"
];

// /!\ avant la boucle $value n'existe pas encore  /!\

foreach($abricot as $key => $value){
    // on désigne $value comme la variable prise à chaque élément du tableau
    // on désigne $key comme la variable prise à chaque key du tableau
    // à chaque tour de boucle $value prend la valeur du fruit suivant
    echo("<br/>".$key." : ".$value);
}

// /!\ après la boucle $value n'existe PLUS  /!\

/*
 *
 ------- affichage ------

nom : abricot
couleur : orange

*/


// ############ foreach tableau 2D ############

$fruits = [
    [
        "nom" => "abricot",
        "couleur" => "orange"
    ],
    [
        "nom" => "banane",
        "couleur" => "jaune"
    ]
];

// /!\ avant la boucle $value n'existe pas encore  /!\

foreach($fruits as $key => $value){
    // on désigne $value comme la variable prise à chaque élément (un fruit nom & couleur) du tableau
    // on désigne $key comme la variable prise à chaque key du tableau
    // à chaque tour de boucle $value prend la valeur du fruit suivant
    echo("<br/>".$key." = (".$value["nom"].";".$value["couleur"].")");
}

// /!\ après la boucle $value n'existe PLUS  /!\

/*
 *
 ------- affichage ------

0 = (abricot;orange)
1 = (banane;jaune)

*/


// ############ foreach tableau 2D ############

$fruits = [
    [
        "nom" => "abricot",
        "couleur" => "orange"
    ],
    [
        "nom" => "banane",
        "couleur" => "jaune"
    ]
];


foreach($fruits as $key => $f){
    // on désigne $f comme la variable prise à chaque élément (un fruit nom & couleur) du tableau
    // à chaque tour de boucle $value prend la valeur du fruit suivant
    echo("### fruit ".$key." ###");
    foreach($f as $nomCaract => $valueCaract){
        echo("<br/>".$nomCaract." : ".$valueCaract);
    }
}


/*
 *
 ------- affichage ------

### fruit 0 ###
nom : abricot
couleur : orange
### fruit 1 ###
nom : banane
couleur : jaune

*/

// ############ accès direct dans un tableau 2D ############

$fruits = [
    [
        "nom" => "abricot",
        "couleur" => "orange"
    ],
    [
        "nom" => "banane",
        "couleur" => "jaune"
    ]
];

echo($fruits[0]["nom"]); //le nom de mon premier fruit