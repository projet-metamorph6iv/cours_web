<?php

//for([initialisation] ; condition de continuité ; [incrementation par itération]){ }
for($i = 0; $i < 10; $i++){
    echo (" ".$i);
}

$i = 0;
for(; $i < 10;){
    echo (" ".$i++);
}


// while(condition de continuité)
$i = 11;
while($i < 10) {
    echo (" ".$i++);
}


// do{      }while(condition de continuité)
do {
    echo("c'est quoi ton prenom ? ");
    $prenom = ""; //rentre son prenom
}while($prenom != "*");


// foreach( tableau as [key =>] value)
$tableau = ["toto", "titi", "tata"];

for($i = 0; $i < count($tableau); $i++){
    echo(" ".$tableau[$i]);
}

foreach($tableau as $prenom ){ //pour chaque tours de boucle $prenom prend la valeur d'un élément du tableau
    echo(" ".$prenom);
}

$personne = [ "prenom" => "Toto", "nom" => "TITI", "age" => 10];
foreach($personne as $key => $carat){
    echo($key.": ".$carat."<br/>");
}

$robin = [
        "prenom" => "Robin",
        "age" => 21,
        "nom" => "COLLAS"
];

$personnes[] = $robin;
/*
personnes = [
    0 => [
            "prenom" => "Robin",
            "age" => 21,
            "nom" => "COLLAS"
        ]
]
*/

$jack = [
    "prenom" => "Jack",
    "age" => 38,
    "nom" => "DUPUIS"
];

$personnes[] = $jack;
/*
personnes = [
    0 => [
            "prenom" => "Robin",
            "age" => 21,
            "nom" => "COLLAS"
        ]
    1 => [
            "prenom" => "Jack",
            "age" => 38,
            "nom" => "DUPUIS"
        ]
]
*/

foreach($personnes as $p){
    echo("<br/>".$p['prenom']." ".$p['nom']." ".$p['age']);
}