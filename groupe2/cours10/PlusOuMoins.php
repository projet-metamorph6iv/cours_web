<?php

session_start();

if(!isset($_SESSION["nombre"])) // isset sert à verifier que la key existe dans le tableau
    $_SESSION["nombre"] = rand(0,100);

if(isset($_GET['proposition'])){
    if($_GET['proposition'] > $_SESSION["nombre"])
        echo("c'est plus petit");
    else if($_GET['proposition'] < $_SESSION["nombre"])
        echo("c'est plus grand");
    else {
        echo("gagné !");
        $_SESSION["nombre"] = rand(0, 100);
    }
}

var_dump($_SESSION);
var_dump($_GET);
var_dump($_POST);
var_dump($_COOKIE);
var_dump($_REQUEST); // fusion des tableaux get et post
var_dump($_FILES); // formulaire qui envoient des fichiers
var_dump($_SERVER); // toute les données relatives au serveur
var_dump($_ENV);


?>

<html>
    <body>
        <form method="get">
            <input type="text" name="proposition"/>
            <input type="submit" value="ok"/>
        </form>
    </body>
</html>
