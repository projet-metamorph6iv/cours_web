Cours de web organisés par Sobraz à destination des membres du forum de Metamorph6iv

############ liens utils pour le cours ##############

Notepad++ :
https://notepad-plus-plus.org/download/v6.9.html

TeamViewer :
https://www.teamviewer.com/en/download/windows/

Hangout :
https://talkgadget.google.com/hangouts/_/g3ihtvbgnvbp7h6gcfrzyrzd5qe?authuser=0&hl=fr

w3schools :
http://www.w3schools.com/tags/

jsfiddle :
https://jsfiddle.net/
