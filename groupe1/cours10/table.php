<?php
/**
 * Created by PhpStorm.
 * User: robin
 * Date: 22/01/2017
 * Time: 14:10
 */

// key => value
$table = array();   // jusqu'à php 5.6
$table = [];        // php 7

$table[] = "first"; // valeur 'first' à l'indice 0
// [0 => "first"]

$table["key"] = "value"; // je donne à la clé 'key' la valeur 'value'
// [0 => "first", "key" => "value"]

$value = $table["key"]; // value vaut "value"

unset($table["key"]); // suppression de "key"
// [0 => "first"]