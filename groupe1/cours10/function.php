<?php
/**
 * Created by PhpStorm.
 * User: robin
 * Date: 22/01/2017
 * Time: 14:23
 */

// éviter les redondances
// simplifier le code & alléger
// centraliser un contrôle de la donnée

function maFonction($arg){

}

function ma_fonction(string $arg) : string{

}

// type en php array, string, int, boolean void

function square(int $number) : int{
    return $number * $number;
}

function afficher_menu(){
    echo "un menu";
}

function afficher_text(string $text, int $nb = 1) : void {
    for($i = 0; $i < $nb; $i++)
        echo($text);
}

afficher_text("mon text", 3);
afficher_text("mon text");
